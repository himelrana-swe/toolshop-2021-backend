# Generated by Django 3.2.8 on 2021-10-31 19:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_id', models.CharField(max_length=50)),
                ('title', models.CharField(max_length=50)),
                ('description', models.TextField()),
                ('price', models.FloatField()),
                ('created_at', models.DateField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.IntegerField(choices=[(-1, 'Not Received'), (0, 'Waiting for Confirmation'), (1, 'Partially Confirmed'), (2, 'Confirmed')], default=-1)),
                ('invoice_long_id', models.CharField(max_length=250)),
                ('address', models.CharField(blank=True, max_length=250, null=True)),
                ('btcvalue', models.IntegerField(blank=True, null=True)),
                ('received', models.IntegerField(blank=True, null=True)),
                ('txid', models.CharField(blank=True, max_length=250, null=True)),
                ('rbf', models.IntegerField(blank=True, null=True)),
                ('created_at', models.DateField(auto_now=True)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='payments.order')),
            ],
        ),
    ]
