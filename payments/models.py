from django.db import models

class Order(models.Model):
    product_id = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    description = models.TextField()
    price = models.FloatField()
    created_at = models.DateField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.title

class Invoice(models.Model):
    STATUS_CHOICES = ((-1,"Not Received"),(0,'Waiting for Confirmation'), (1,"Partially Confirmed"), (2,"Confirmed"))

    order = models.ForeignKey("Order", on_delete=models.CASCADE)
    status = models.IntegerField(choices=STATUS_CHOICES, default=-1)
    invoice_long_id = models.CharField(max_length=250)
    address = models.CharField(max_length=250, blank=True, null=True)
    btcvalue = models.IntegerField(blank=True, null=True)
    received = models.IntegerField(blank=True, null=True)
    txid = models.CharField(max_length=250, blank=True, null=True)
    rbf = models.IntegerField(blank=True, null=True)
    created_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.address